package GUI;

import User.Args;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;

public class GUI {

    private final Args args;
    private final JFrame frame;
    private final JPanel barsPanel;
    private final JLabel iterationLabel;
    private final JLabel timeLabel;

    public GUI(Args args) {
        this.args = args;
        frame = new JFrame("Lázaro SAV");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1280, 720);
        frame.setLayout(new BorderLayout());

        barsPanel = new JPanel();
        frame.add(barsPanel, BorderLayout.CENTER);

        iterationLabel = new JLabel("Iterations: 0", SwingConstants.CENTER);
        iterationLabel.setFont(new Font("Arial", Font.PLAIN, 24));
        frame.add(iterationLabel, BorderLayout.NORTH);

        timeLabel = new JLabel("Time: 0 seconds", SwingConstants.CENTER);
        timeLabel.setFont(new Font("Arial", Font.PLAIN, 24));
        frame.add(timeLabel, BorderLayout.SOUTH);

        frame.setVisible(true);
    }

    public void updateBars(int[] arr) {
        barsPanel.removeAll();
        barsPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 20, 10));

        int maxValue = args.isRandomInput() ? 1000 : findMaxValue(arr);

        for (int j : arr) {
            String valueToDisplay = getString(j);

            int originalBarHeight = (int) (((double) j / maxValue) * (frame.getHeight() - 100));
            int barHeight = (int) (originalBarHeight * 10.0);

            JPanel barAndLabelPanel = new JPanel();
            barAndLabelPanel.setLayout(new BorderLayout());

            JPanel barPanel = new JPanel();
            barPanel.setPreferredSize(new Dimension(30, barHeight)); //largura da barra
            barPanel.setBackground(Color.blue);

            JLabel label = new JLabel(valueToDisplay, SwingConstants.CENTER);

            barAndLabelPanel.add(barPanel, BorderLayout.SOUTH);
            barAndLabelPanel.add(label, BorderLayout.NORTH);

            barsPanel.add(barAndLabelPanel);
        }

        frame.revalidate();
        frame.repaint();
    }


    private String getString(int j) {
        String valueToDisplay = "";

        if (args.getType().equals("n")) {
            valueToDisplay = String.valueOf(j);
        } else if (args.getType().equals("c")) {
            if (j >= 0 && j <= 127) {
                valueToDisplay = String.valueOf((char) j);
            }
        }
        return valueToDisplay;
    }


    public void updateCount(int count) {
        iterationLabel.setText("Iterations: " + count);
    }

    public void updateTime(String time) {
        timeLabel.setText("Time: " + time);
    }

    private int findMaxValue(int[] arr) {
        return Arrays.stream(arr).max().orElse(Integer.MIN_VALUE);
    }

    public int getSpeed() {
        return args.getSpeed();
    }

    public String getOrder() {
        return args.getOrder();
    }
}
