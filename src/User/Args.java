package User;
public class Args {
    private String type;
    private int algorithm;
    private String order;
    private int[] values;
    private int speed;
    private String inputType;

    public Args(String[] args) {
        for (String arg : args) {
            String[] parts = arg.split("=");
            if (parts.length != 2) {
                System.out.println("Invalid argument: " + arg);
                continue;
            }

            String key = parts[0].trim();
            String value = parts[1].trim();

            switch (key) {
                case "t" -> {
                    if (value.equals("n") || value.equals("c")) {
                        type = value;
                    } else {
                        System.out.println("Invalid type: " + value);
                    }
                }
                case "a" -> {
                    try {
                        algorithm = Integer.parseInt(value);
                    } catch (NumberFormatException e) {
                        System.out.println("Invalid algorithm: " + value);
                    }
                }
                case "o" -> {
                    if (value.equals("ZA") || value.equals("AZ")) {
                        order = value;
                    } else {
                        System.out.println("Invalid order: " + value);
                    }
                }
                case "in" -> {
                    if (value.equals("m") || value.equals("r")) {
                        inputType = value;
                    } else {
                        System.out.println("Invalid input: " + value);
                    }
                }
                case "v" -> {
                    assert type != null;
                    if (type.equals("n")) {
                        String[] valueStrings = value.split(",");
                        values = new int[valueStrings.length];
                        try {
                            for (int i = 0; i < valueStrings.length; i++) {
                                values[i] = Integer.parseInt(valueStrings[i]);
                            }
                        } catch (NumberFormatException e) {
                            System.out.println("Invalid value: " + value);
                        }
                    } else if (type.equals("c")) {
                        String[] valueStrings = value.split(",");
                        values = new int[valueStrings.length];
                        try {
                            for (int i = 0; i < valueStrings.length; i++) {
                                char letter = valueStrings[i].trim().charAt(0);
                                values[i] = letter;
                            }
                        } catch (NumberFormatException e) {
                            System.out.println("Invalid value: " + value);
                        }
                    }
                }
                case "s" -> {
                    try {
                        speed = Integer.parseInt(value);
                    } catch (NumberFormatException e) {
                        System.out.println("Invalid speed: " + value);
                    }
                }
                default -> System.out.println("Invalid argument: " + arg);
            }
        }
    }

    public String getType() {
        return type;
    }

    public int getAlgorithm() {
        return algorithm;
    }

    public String getOrder() {
        return order;
    }

    public int[] getValues() {
        return values;
    }

    public int getSpeed() {
        return speed;
    }

    public boolean isRandomInput() {
        return "r".equals(inputType);
    }
}
