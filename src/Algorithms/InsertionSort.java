package Algorithms;

import javax.swing.*;
import GUI.GUI;


public class InsertionSort implements SortAlgorithm {

    long startTime = System.currentTimeMillis() / 1000;

    public void sort(GUI gui, int[] arr) {
        int n = arr.length;

        for (int i = 1; i < n; i++) {
            int key = arr[i];
            int j = i - 1;

            while (j >= 0 && ((gui.getOrder().equals("AZ") && arr[j] > key) || (gui.getOrder().equals("ZA") && arr[j] < key))) {
                arr[j + 1] = arr[j];
                j = j - 1;

                final int currentIteration = i;
                SwingUtilities.invokeLater(() -> {
                    gui.updateBars(arr);
                    gui.updateCount(currentIteration);
                });

                try {
                    Thread.sleep(gui.getSpeed());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            arr[j + 1] = key;

            final int currentIteration = i;
            SwingUtilities.invokeLater(() -> {
                gui.updateBars(arr);
                gui.updateCount(currentIteration);
            });
        }

        long endTime = System.currentTimeMillis() / 1000;
        long totalTime = endTime - startTime;
        String formattedTime = String.format("%d seconds", totalTime);
        gui.updateTime(formattedTime);
    }
}
