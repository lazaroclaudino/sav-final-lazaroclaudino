package Algorithms;
import GUI.GUI;

public interface SortAlgorithm {

    void sort(GUI gui, int[] arr);
}
