import Algorithms.BubbleSort;
import Algorithms.InsertionSort;
import Algorithms.SelectionSort;
import Algorithms.SortAlgorithm;
import GUI.GUI;
import User.Args;

import java.util.Random;

public class SAV {

    public static void main(String[] args) {
        Args argsObj = new Args(args);
        GUI gui = new GUI(argsObj);
        int[] arr;

        if (argsObj.isRandomInput()) {
            arr = generateRandomArray();
        } else {
            arr = argsObj.getValues();
        }

        SortAlgorithm sortAlg;

        switch (argsObj.getAlgorithm()) {
            case 1 -> sortAlg = new BubbleSort();
            case 2 -> sortAlg = new InsertionSort();
            case 3 -> sortAlg = new SelectionSort();
            default -> {
                System.out.println("Invalid Algorithm!");
                return;
            }
        }

        sortAlg.sort(gui, arr);
    }

    private static int[] generateRandomArray() {
        int[] randomArray = new int[10];
        Random random = new Random();

        for (int i = 0; i < 10; i++) {
            randomArray[i] = random.nextInt((100 - 1) + 1) + 1;
        }

        return randomArray;
    }
}
