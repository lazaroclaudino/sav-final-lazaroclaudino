# Projeto SAV - Lázaro Landerson

## Descrição

Essa é a minha implementação dos algoritmos de ordenação: BubbleSort, InsertionSort e SelectionSort.

- *BubbleSort*: é um algoritmo de ordenação simples que compara elementos adjacentes e troca os que estão em ordem errada.

- *InsertionSort*: funciona comparando cada elemento da lista com os elementos anteriores e movendo o elemento para a posição correta se ele estiver fora de ordem.

- *SelectionSort*: é um algoritmo de ordenação que funciona comparando os elementos da lista e selecionando o menor elemento em cada iteração. O menor elemento é então movido para o início da lista, e o processo é repetido até que todos os elementos estejam ordenados.

## Como executar o programa

1. Certifique-se de ter o Java instalado em sua máquina.
2. Clone este repositório para o seu computador.
3. Navegue até o diretório do projeto.
4. Compile o programa utilizando o comando: `javac SAV.java`
5. Veja o exemplo de execução
## Funcionamento do programa

Após executar o programa com os valores desejados, ele solicitará que você escolha qual algoritmo de ordenação deseja utilizar para ordenar os valores fornecidos. Caso insira uma letra em vez de um número correspondente ao algoritmo, ocorrerá um erro.

## Exemplo de execução

java SAV t=c a = 1 o=za in=m v=a,b,c,d,e s=1000

- `TIPO`: Escolha o tipo de entrada (use `n` para NUMÉRICO ou `c` para CARACTERE).
- `ALGORITMO`: Escolha o algoritmo de ordenação (use `1` para Bubble Sort, `2` para Selection Sort ou `3` para Insertion Sort).
- `ORDEM`: Especifique a ordem da ordenação (use `AZ` para CRESCENTE ou `ZA` para DECRESCENTE).
- `ENTRADA`: Escolha o método de entrada (use `m` para MANUAL ou `r` para RANDÔMICO).
- `VALORES`: Fornecer o vetor completo a ser ordenado (por exemplo, `1,2,3,4,5,6` ou `a,b,c,d,e,d,a,d`).
- `VELOCIDADE`: Defina a velocidade de ordenação em milissegundos.